import bcrypt, { genSaltSync } from "bcryptjs";
import jwt from "jsonwebtoken";

import { secretOrPrivateKey } from "../config";

import Users from "../models/Users";

import "../middlewares/verifyjwt";

export default {
  async SingUp(req, res) {
    try {
      // crea bcrypt hash en el password
      req.body.password = await bcrypt.hash(req.body.password, genSaltSync(10));
      // crea user y el token
      const { _id } = await Users.create(req.body);
      const token = jwt.sign({ _id }, secretOrPrivateKey, {
        expiresIn: "1day",
      });

      res.status(200).json({ token });
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },

  async SingIn(req, res) {
    try {
      const userValidate = await Users.findOne({
        email: req.body.email,
      }).populate("roles");

      if (!userValidate) {
        return res.status(404).json({ message: "User not found" });
      }

      userValidate.roles.map((x) => {
        if (x.name !== "Admin") {
          if (!userValidate.states) {
            return res.status(401).json({ message: "Not authorized" });
          }
        }
      });

      const UserCompare = await bcrypt.compare(
        req.body.password,
        userValidate.password
      );

      if (!UserCompare) {
        return res.status(401).json({ message: "Password not found" });
      }

      const token = jwt.sign({ _id: userValidate.id }, secretOrPrivateKey, {
        expiresIn: "1day",
      });

      res.json({ token });
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },
};
