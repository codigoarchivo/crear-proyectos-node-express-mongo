import { Router } from "express";
import user from "../controllers/user.controller";
import authjwt from "../middlewares/authjwt";

const routes = Router();

routes.post("/", [authjwt.verifyToken, authjwt.isAdmin], user.createUser);

export default routes;
