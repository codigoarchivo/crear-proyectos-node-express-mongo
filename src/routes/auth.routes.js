import auth from "../controllers/auth.controllers";
import check from "../middlewares/verifyjwt";
import { Router } from "express";

const routes = Router();

routes.post(
  "/sing-up",
  check.UsernameOrEmailOrRole,
  check.listDefaultRoleCompareRoleIdStates,
  auth.SingUp
);
routes.post("/sing-in", auth.SingIn);

export default routes;
