import express from "express";
import morgan from "morgan";
import { crerRoles } from "./libs/initialSetup";
import { VERSION } from "./config";

import auth from "./routes/auth.routes";
import user from "./routes/user.routes";

const app = express();

crerRoles();

app.use(morgan("dev"));
app.use(express.json());

app.get("/", (req, res) => {
  res.json("Hola Mundo");
});

app.use(`/api/${VERSION}/auth`, auth);
app.use(`/api/${VERSION}/user`, user);

export default app;
