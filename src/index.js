import app from "./app";
import { port, VERSION } from "./config";
import "./database";

app.listen(port, () => {
  console.log(`Version: ${VERSION}`);
  console.log(`Conectado al puerto: ${port}`);
});
