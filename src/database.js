import mongoose from "mongoose";
import { HOST, portdb } from "./config";

mongoose
  .connect(`mongodb://${HOST}/dbProyect`)
  .then((res) => console.log(`Conectado base dato: ${portdb}`))
  .catch((err) => console.log(err));
