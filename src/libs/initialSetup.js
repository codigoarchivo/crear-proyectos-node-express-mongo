import Rol from "../models/Roles";

module.exports = {
  async crerRoles(req, res) {
    try {
      const count = await Rol.estimatedDocumentCount();
      if (count > 0) return;
      const values = await Promise.all([
        Rol.create({ name: "Admin" }),
        Rol.create({ name: "Moderator" }),
        Rol.create({ name: "User" }),
      ]);
      console.log(values);
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },
};
