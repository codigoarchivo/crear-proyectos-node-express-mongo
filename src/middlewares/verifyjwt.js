import Roles from "../models/Roles";
import Users from "../models/Users";

export default {
  async UsernameOrEmailOrRole(req, res, next) {
    try {
      const username = await Users.findOne({ username: req.body.username });
      if (username) {
        return res.status(400).json({ msg: "The user already exists" });
      }

      const email = await Users.findOne({ email: req.body.email });
      if (email) {
        return res.status(400).json({ msg: "The email already exists" });
      }

      next();
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },

  async listDefaultRoleCompareRoleIdStates(req, res, next) {
    try {
      //trae los roles de la base datos
      let conectar = [];
      const r = await Roles.find();
      r.map((x) => conectar.push(x.name));

      // si no viene nada se agrega User por default
      if (!req.body.roles) {
        req.body.roles = ["User"];
      }

      // verifica si concuerda con la base datos
      req.body.roles.map((x) => {
        if (!conectar.includes(x)) {
          return res.status(400).json({
            msg: `Role ${x} does not exists`,
          });
        }
      });

      // trae los id roles y agrega estates
      const rol = await Roles.find({ name: { $in: req.body.roles } });
      req.body.roles = rol.map((x) => x._id); //solo asignado;
      rol.map((x) => (x.name === "Admin" ? false : (req.body.states = 1))); //estado = 1;

      next();
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },
};
