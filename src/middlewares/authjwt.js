import jwt from "jsonwebtoken";
import { secretOrPrivateKey } from "../config";
import Roles from "../models/Roles";
import Users from "../models/Users";

export default {
  async verifyToken(req, res, next) {
    try {
      const token = req.headers["x-access-token"];

      if (!token) {
        return res.status(403).json({ msg: "No token provided" });
      }

      const { _id } = jwt.verify(token, secretOrPrivateKey);
      req.userId = _id;

      const user = await Users.findById({ _id, password: 0 });

      if (!user) {
        return res.status(404).json({ msg: "No user found" });
      }
      next();
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },

  async isAdmin(req, res, next) {
    try {
      const { roles } = await Users.findById(req.userId);
      const rol = await Roles.find({ _id: { $in: roles } });
      rol.map((x) => {
        if (x.name !== "Admin") {
          return res.status(403).json({ msg: "Required Admin role" });
        }
      });
      next();
      return;
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },

  async isModerator(req, res, next) {
    try {
      const { roles } = await Users.findById(req.userId);
      const rol = await Roles.find({ _id: { $in: roles } });
      rol.map((x) => {
        if (x.name !== "Moderator") {
          return res.status(403).json({ msg: "Required Moderator role" });
        }
      });
      next();
      return;
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },

  async isUser(req, res, next) {
    try {
      const { roles } = await Users.findById(req.userId);
      const rol = await Roles.find({ _id: { $in: roles } });
      rol.map((x) => {
        if (x.name !== "User") {
          return res.status(403).json({ msg: "Required User role" });
        }
      });
      next();
      return;
    } catch (error) {
      return res.status(500).json({
        msg: `>> ${error} <<`,
      });
    }
  },
};
